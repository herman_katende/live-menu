# Overview

This is my submission for the AP CSP Create Task 2023

The purpose of this program is to provide low-cost smart menu applications for small businesses. An Airtable is used to keep track of the styling for the menus, and a seperate one is used for controlling the different menu items and their ingredients. The application can be scaled, the only limit being how many screens you can fit in your business.

# Setup and Installation

## Airtable Setup 

1. If you haven't already done so, head over to [Airtable](https://airtable.com/) and make an account.

2. Go ahead and make a new workspace and name it whatever you would like. I named mine after the resturant I am installing for.

3. Make a copy of both of these bases:
>  [Airtable Base (Menu)](https://airtable.com/shr4HOR5AABNtSH6r)

>[Airtable Base (Styles)](https://airtable.com/shrzXJOpzS86wPEK0)

## Display Setup

Requirements:
* A device to run the application on
    * Must have python installed (Version 3.2 or newer)
    * An internet connection
    * A web browser, newer than IE11. All modern browsers should work.

1. (Steps 1 and 2 are for Raspberry Pi. Skip to step 3 otherwise)
Start with a fresh install of Raspbian. Use Rasperry Pi Imager or Balena Etcher to flash the OS of your choice.

[How to use Raspberry Pi Imager](https://raspberrytips.com/raspberry-pi-imager-guide/)

2. Connect to your Wi-Fi or Ethernet Connection. You can do this using Raspberry Pi Imager, or wpa_supplicant.conf

[How to set up WiFi on Raspberry Pi using wpa_supplicant.conf](https://www.raspberrypi-spy.co.uk/2017/04/manually-setting-up-pi-wifi-using-wpa_supplicant-conf/)

3. Once your device is connected to the network, enable SSH on it. This varies by distribution. For Windows and Mac, skip this step.

4. Installation
#### Download Files 
```bash
git clone https://gitlab.com/beanbean1706/live-menu.git
```
Change directories into the correct folder
```bash
cd live-menu/WebServer
```
Use nano to open up `main.js` (Configuration file coming soon)
```bash
nano main.js
```
#### Configure

On a different device, open up the [Airtable PAT Creation](https://airtable.com/create/tokens) page

Here, you will generate a Personal Access Token to use for authenticating app. Click "Create New Token"

![Create New Token Button](https://res.cloudinary.com/dhwwyftkb/image/upload/v1684873714/Screenshot_2023-05-23_152244_d301tr.png)

Create a new token with a name that matches your use case. Allow the token access to the scopes `data.records:read` and `schema.bases.read`

Also allow the token access to the two bases you just copies, Styles and Menu

![Screenshot Showing the Scopes and Acces Settings](https://res.cloudinary.com/dhwwyftkb/image/upload/v1684874052/Screenshot_2023-05-23_153309_gx9wd3.png)

Copy your PAT down, as you will not be able to view it again later from your Airtable dashboard.

Go back to your device, which should still have `main.js` open in nano. At the top of the file, there is a line declaring a variable called "PAT_token." Put your PAT inside the quotes.

```javascript
var PAT_token = "" // Your Airtable Token Here
```
If you renamed the "PI1" sheet to be something different, now is the time to make those changes. On the fourth line of `main.js` change "PI1" with whatever you changed the name to.
```javascript
let WhatMenu = "PI1"
```

Save and close the file by pressing `CTRL + X` then `Y` and finally `ENTER`

Now, do the same thing for `applyStyles.js` and `functions.js`

```bash
nano applyStyles.js
```
As before, place your PAT in between the quotes.
```javascript
var PAT_token = "" // Your Airtable Token Here
```
`CTRL + X` then `Y` then `ENTER`

```bash
nano functions.js
```
As before, place your PAT in between the quotes.
```javascript
var PAT_token = "" // Your Airtable Token Here
```
`CTRL + X` then `Y` then `ENTER`

#### Set Base IDs

Open your "Menu" Airtable sheet. Click Help > API Documentation.

![Help Button](https://res.cloudinary.com/dhwwyftkb/image/upload/v1684874750/Screenshot_2023-05-23_154452_fuws2e.png)

![API Documentation Button](https://res.cloudinary.com/dhwwyftkb/image/upload/v1684874750/Screenshot_2023-05-23_154505_ewrwtn.png)

Find on the page where your base's ID is listed. 

![Base ID](https://res.cloudinary.com/dhwwyftkb/image/upload/v1684874898/Screenshot_2023-05-23_154710_lbqner.png)

Copy the Base ID. Go back to `functions.js` and put it in the variable `MenuBaseID` on line 2.
```javascript
MenuBaseID = "" // ID to your Base
```
Go back to your Styles base, and go to Help > API Documentation. Copy the Base ID for your Styles base. Put it in the variable `StylesBaseID` in `ApplyStyles.js`

## Usage

Open the "Menu" Base. Notice there are three table views created.

![Screenshot Showing 3 Tabs, PI1, Stock1, Stock2](https://res.cloudinary.com/dhwwyftkb/image/upload/v1684868395/Screenshot_2023-05-23_135931_dosvey.png)

These are the main tables you will use to change your menu board. The PI1 Table can be renamed, just make sure to remember what you name it to, and update it accordingly.

![Screenshot Showing 3 Tabs, PI1, Stock1, Stock2](https://res.cloudinary.com/dhwwyftkb/image/upload/v1684868826/Screenshot_2023-05-23_140635_q7rwhl.png)

You can change the name of items, change the notes on the items, the ingredients, and manually check it as on the menu or not. You can switch over to the Stock1 and Stock2 tabs to add to your list of ingredients, and mark it if it's in stock or not. If you mark an item at out of stock, any items with that ingredient will be removed from the list. Currently, there is a maximum ingredient count of 99 per each of Stock1 and Stock2. More will be coming in the future. 

In the Styles base, you have all of your options on how you would like to style your menu. 

![](https://res.cloudinary.com/dhwwyftkb/image/upload/v1684876168/Screenshot_2023-05-23_160908_qqmkjg.png)

Here, you can change the fonts, background image, and overall formatting. If you changed the name of the table in your Menu base, make sure to adjust the Name column in the Styles sheet, as well as the name of the styles sheet.

![](https://res.cloudinary.com/dhwwyftkb/image/upload/v1684876168/Screenshot_2023-05-23_160916_efxr07.png)

## Running

To start the app, simply navigate to the folder all of the files are contained in. Then, run
```bash
python3 -m http.server
```
This will start a local server on your device. Simply navigate your web browser to `127.0.0.1` and it will display the board. The menu can also be shown on other devices on the network, just replace the IP with your device's local IP.

## Known Issues / To-Do List

* Add automatic refresh
* Add a configuration file instea of changing variables inside the files
* Make a server / client app type thing

## License

Licensed under Creaive Commons CC BY-ND 4.0

[![](https://i.creativecommons.org/l/by-nd/4.0/88x31.png)](https://creativecommons.org/licenses/by-nd/4.0/legalcode)

