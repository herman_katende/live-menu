var PAT_token = "" // Your Airtable Token Here
MenuBaseID = "" // ID to your Base
//This file is to manage all of the functions I have created for my program. They are created here, initialized before anything else, then referenced in main.js

//initialize an undefined variable
var emptyVar;

// apiRequest() function
// Pass the endpoint and receive the JSON that is returned
// Modified from code given by Quentin at https://stackoverflow.com/a/4033310

function apiRequest(ENDPOINT) {
    var http_request = new XMLHttpRequest();
    http_request.open("GET", "https://api.airtable.com/v0/" + MenuBaseID + ENDPOINT + "", false);
    http_request.setRequestHeader("Authorization","Bearer " + PAT_token); // My authentication token has been removed for submission
    http_request.send(null);
    jsontext = http_request.responseText;
    return JSON.parse(jsontext);}

// end apiRequest()

// Custom Functions
// the checkItem() function is fully written by myself
// pass it a JavaScript list object containing ingredients, a boolean value, and a JavaScript list containing disabled items

function checkItem(ingredientsList, onMenu, disabledItemList) {
    let isItemAllowed = true;

    for (j = 0; j < ingredientsList.length; j++) {if (disabledItemList.includes(ingredientsList[j])) {isItemAllowed = false;}}
    if (isItemAllowed && onMenu) {return true;}
    else {return false;}}

//end checkItem()

// createNewMenuElement()
// pass it a text value to set the Item's name to, and getElementByID("") of the <ol> to append to
// Various w3schools.com articles used to create this function, such as:
// https://www.w3schools.com/jsref/met_document_createtextnode.asp
// https://www.w3schools.com/jsref/met_document_createelement.asp

function createNewMenuElement(text, desc, list) {
    var bold = document.createElement("b");
    var newtext = document.createTextNode(text); 
    bold.appendChild(newtext);
    if (typeof(desc) != "undefined") {
        var newdesc = document.createTextNode(desc);
    }
    var listitem = document.createElement("li");
    var newline = document.createElement("br");
    var newline2 = document.createElement("br");
    listitem.appendChild(bold); 
    listitem.appendChild(newline);
    if (typeof(desc) != "undefined") {
        listitem.appendChild(newdesc);
    }
    list.appendChild(listitem);
    list.appendChild(newline2)
}