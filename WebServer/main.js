var PAT_token = "" // Your Airtable Token Here
//WHAT MENU BOARD IS THIS?
//This is to have the app reference a specific table on Airtable
let WhatMenu = "PI1"
//\\ ITEMS CHECKS //\\
stock1 = apiRequest("/Stock1")["records"]; //Get the first stock table
stock2 = apiRequest("/Stock2")["records"]; //Get the second stock table
let fullIngredientsList = [].concat(stock1, stock2); //Combine the stock tables into one list called "Items"
// I have to get two seperate tables for stocked items, due to a limitation in Airtable that doesn't allow the API to get a table greater than 100 items.
// At the time I am writing this, there are 126 different ingredients that are used.
const disabledItems = []; // Create the list of items that are marked as out of stock
for (let i = 0; i < fullIngredientsList.length; i++) { //For every ingredient, loop through once
    if (typeof(fullIngredientsList[i]["fields"]["In Stock?"]) == "undefined") { //if the typeof() of the ingredients's "In Stock?" field is "undefined" (unchecked)
        console.log(fullIngredientsList[i]["fields"]["Name"]); 
        disabledItems.push(fullIngredientsList[i]["fields"]["Name"]);}} //Add the ingredient name to the list "disabledItems" because it is marked as out of stock
// At this point in the program, the "disabledItems" list is fully populated with every ingredient marked as out of stock
menuApiReq = apiRequest("/" + WhatMenu + "") //Make an API call for the endpoint specified in the variable "WhatMenu"
for(var i = 0; i < apiRequest("/" + WhatMenu + "")["records"].length; i++) { // For every item, loop once
    itemIngredients = menuApiReq["records"][i]["fields"]["Ingredients"]; // set the variable "itemIngredients" to the list of ingredients from the menu api request
    onMenu = menuApiReq["records"][i]["fields"]["On Menu?"]; // set the variable "onMenu" to the state of the On Menu? field from the api request
    if (checkItem(itemIngredients, onMenu, disabledItems)) { // call the function checkItem() and pass the previously created variables
        //If it is true, call the function createNewMenuElement() and fill it accordingly
        createNewMenuElement(menuApiReq["records"][i]["fields"]["Name"], menuApiReq["records"][i]["fields"]["Notes"], document.getElementById("ItemsOL"));}}