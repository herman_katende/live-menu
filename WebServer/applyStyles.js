var PAT_token = "" // Your Airtable Token Here
StylesBaseID = "" // ID to your Base
// applyStyles.js
// Read Styles from the airtable table and apply them to the page
// Reference this AFTER functions.js and main.js, to apply styles after page load

//stylesRequest() uses code modified from and answer by Quentin at https://stackoverflow.com/a/4033310
function stylesRequest(ENDPOINT){
var xmlHttp = new XMLHttpRequest();xmlHttp.open( "GET", "https://api.airtable.com/v0/" + StylesBaseID + ENDPOINT + "", false );
xmlHttp.setRequestHeader("Authorization","Bearer " + PAT_token); // My authentication token has been removed for submission
xmlHttp.send( null );
jsontext = xmlHttp.responseText;
return JSON.parse(jsontext)}

// Various Algorithms and if statements to set the proper variables to the proper value from the Airtable

let stylesReq = stylesRequest("/" + WhatMenu + "Styles")["records"];
console.log(stylesReq);
for (var i = 0; i < stylesReq.length; i++) {
    if (stylesReq[i]["fields"]["Name"] == WhatMenu && stylesReq[i]["fields"]["desc"] == "font") {
    fontUrl = stylesReq[i]["fields"]["File"][0]["url"];
}}
for (var i = 0; i < stylesReq.length; i++) {
    if (stylesReq[i]["fields"]["Name"] == WhatMenu && stylesReq[i]["fields"]["desc"] == "headerfont") {
    headerfontUrl = stylesReq[i]["fields"]["File"][0]["url"];
}}
for (var i = 0; i < stylesReq.length; i++) {
    if (stylesReq[i]["fields"]["Name"] == WhatMenu && stylesReq[i]["fields"]["desc"] == "colCount") {
    colCount = stylesReq[i]["fields"]["txt"];
}
if (stylesReq[i]["fields"]["Name"] == WhatMenu && stylesReq[i]["fields"]["desc"] == "bgpic") {
    bgPic = stylesReq[i]["fields"]["File"][0]["url"];
}
if (stylesReq[i]["fields"]["Name"] == WhatMenu && stylesReq[i]["fields"]["desc"] == "header") {
    document.getElementById("header").innerHTML = stylesReq[i]["fields"]["txt"];
}
if (stylesReq[i]["fields"]["Name"] == WhatMenu && stylesReq[i]["fields"]["desc"] == "macrosLeft") {
    createNewMenuElement(stylesReq[i]["fields"]["txt"], "", document.getElementById("macrosL"));
}
if (stylesReq[i]["fields"]["Name"] == WhatMenu && stylesReq[i]["fields"]["desc"] == "macrosRight") {
    createNewMenuElement(stylesReq[i]["fields"]["txt"], "", document.getElementById("macrosR"));
}
}

var styler = document.createElement('style');

// CSS CODE TO INJECT
// This is the templace CSS code that will be added to the page
// add a variable to the code to have it replaced automatically

styler.appendChild(document.createTextNode("\
@font-face {\
    font-family: 'AirtableSetFont';\
    src: url('" + fontUrl + "') format('truetype');\
}\
@font-face {\
    font-family: 'AirtableSetHeaderFont';\
    src: url('" + headerfontUrl + "') format('truetype');\
}\
html {\
font-family: AirtableSetFont;\
background-image: url(" + bgPic + ");\
background-position: center center;\
background-repeat: no-repeat;\
background-attachment: fixed;\
background-size: cover;\
}\
#header {\
    font-family: AirtableSetHeaderFont;\
    font-size: 75px;\
    margin-top: 0px;\
    margin-bottom: 0px;\
    padding: 0px;\
}\
.macros {\
    font-family: AirtableSetHeaderFont;\
    margin-top: 0px;\
    font-size: 25px;\
    margin-bottom: 0px;\
    padding: 0px;\
}\
#ItemsOL {\
    column-count: " + colCount + ";\
    text-align: center;\
    color: #fff;\
    font-size: 22px;\
    list-style-type: none;\
    line-height: 1.25;\
}\
"));

document.head.appendChild(styler);